package sistemaexperto;

// Importaciones y código de la clase aquí

import java.util.ArrayList;

// Clase que gestiona la base de hechos
class BaseDeHechos {
    // Lista de los hechos
    protected ArrayList<IHecho> hechos; 

    public ArrayList<IHecho> getHechos() { 
        return hechos; 
    }

    // Constructor
    public BaseDeHechos() { 
        hechos = new ArrayList<>(); // Usar diamond operator <>
    }

    // Vaciar la base
    public void Vaciar() {
        hechos.clear();
    }

    // Agregar un hecho
    public void AgregarHecho(IHecho hecho) {
        hechos.add(hecho); // Usar el parámetro 'hecho' en lugar de 'fait'
    }

    // Buscar un hecho a partir de su nombre, devuelve null si no existe
    public IHecho Buscar(String nombre) {
        for (IHecho hecho : hechos) {
            if (hecho.Nombre().equals(nombre)) {
                return hecho;
            }
        }
        return null;
    }

    // Busca el valor de un hecho, devuelve null si el hecho no existe
    public Object RecuperarValorHecho(String nombre) {
        for (IHecho hecho : hechos) {
            if (hecho.Nombre().equals(nombre)) {
                return hecho.Valor();
            }
        }
        return null;
    }
}
