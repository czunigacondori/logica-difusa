package busquedaCaminos;

// Arc en el grafico
public class Arco {
    protected Nodo origen;
    protected Nodo destino;
    protected double cout;
    
    public Arco(Nodo _origen, Nodo _destino, double _cout) {
        origen = _origen;
        destino = _destino;
        cout = _cout;
    }
}
