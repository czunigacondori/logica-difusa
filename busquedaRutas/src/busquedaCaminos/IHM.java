package busquedaCaminos;

// Interface que representa las salidas del programa
public interface IHM {
    void MostrarResultado(String camino, double distancia);
}
